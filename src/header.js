import React, { Component } from 'react';
import './css/bootstrap.css';
import './font-awesome/css/font-awesome.min.css';
import './css/animate.css';
import './css/style.css';
import './css/default.css';

import icons1 from './img/icons/lock.png';
import icons2 from './img/icons/shield.png';
import icons3 from './img/icons/picture.png';
import icons4 from './img/icons/calculator.png';
import icons5 from './img/icons/shopping-online.png';
import icons6 from './img/icons/bar-chart.png';
import icons7 from './img/icons/cloud-computing.png';
import icons8 from './img/icons/computer.png';
import icon_like from './img/icons/like.png';

import portofolios1 from './img/portofolio/portofolio1.JPG';
import portofolios2 from './img/portofolio/portofolio2.JPG';
import portofolios3 from './img/portofolio/portofolio3.JPG';
import portofolios4 from './img/portofolio/portofolio4.JPG';
import portofolios5 from './img/portofolio/portofolio5.JPG';

import profil from './img/profil/afifur.JPG';

class Header extends Component {
	
	render() {
	    return (
	    	<div className="data">
		    	<nav className="navbar navbar-custom navbar-fixed-top" role="navigation">
			        <div className="container">
			            <div className="navbar-header page-scroll">
			                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
			                    <i className="fa fa-bars"></i>
			                </button>
			                <a className="navbar-brand" href="">
			                    <h1>AFIFUR RAHMAN</h1>
			                </a>
			            </div>
			            <div className="collapse navbar-collapse navbar-right navbar-main-collapse">
					      <ul className="nav navbar-nav">
					        <li className="active"><a href="#intro"><span className="glyphicon glyphicon-home"></span> Home</a></li>
					        <li><a href="#about">About Me</a></li>
							<li><a href="#service">Skill</a></li>
							<li><a href="#portofolio">Portofolio</a></li>
							<li><a href="#contact">Contact</a></li>
					      </ul>
					    </div>
			        </div>
			    </nav>
			    <section id="intro" className="intro">
					<div className="slogan">
						<h2> <span className="text_color">MY PERSONAL WEBSITE</span> </h2>
						<h4> Everything will come to those who keep trying with determination and patience</h4>
						<h5>- Edison</h5>
					</div>
					<div className="page-scroll">
						<br />
					</div>
			    </section>
		    	<section id="about" className="home-section text-center">
		    		<div className="heading-about">
		    			<div className="container">
		    				<div className="row">
		    					<div className="col-lg-8 col-lg-offset-2">
									<div className="wow bounceInDown" data-wow-delay="0.4s">
										<div className="section-heading">
											<h2>About Me</h2>
											<i className="fa fa-2x fa-angle-down"></i>
										</div>
									</div>
								</div>
		    				</div>
		    			</div>
		    		</div>
		    		<div className="container">
		    			<div className="row">
							<div className="col-lg-2 col-lg-offset-5">
								<hr className="marginbot-50" />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-2 col-md-2">
								<img src={profil} alt="" className="img-circle style-imgs" />
							</div>
							<div className="col-sm-10 col-md-10">
								<div className="styleText">
									<p>
										<b>Hi</b>, my name is Afifur Rahman and you can call me Afif, 
										I was born in Demak in Central Java. 
										I studied at Telkom University in major computerized accounting. 
										That was awesome three years in Bandung. Not to long after my graduation day, 
										I already got my first job in PT. Apta Media Indonesia in Jakarta as a Web Developer till now. 
										My live is not just about coding and working. In my leisure time i like to play guitar, 
										do futsal with my friends and also play PES.
									</p>
									<p>
										I was familiar some technologies like : php , framework codeigniter , framework yii2 , database mysql & postgreesql , javascript , jquery , html + css & boostrap . 
										I have expertise in developing accounting system based program . Other than that I was involved make a create marketplace system & reservation system online ticket.
									</p>
									<p>
										If you are interested in develop system that appropriate my skills , Please contact me by email <a href="#emails"><b> below this </b></a>.
									</p>
									<p>
										Thankyou &nbsp; <img src={icon_like} alt="" />  &nbsp; <img src={icon_like} alt="" />
									</p>
								</div>
							</div>
						</div>
		    		</div>
		    	</section>
		    	<section id="service" className="home-section text-center bg-gray">
					<div className="heading-about">
						<div className="container">
						<div className="row">
							<div className="col-lg-8 col-lg-offset-2">
								<div className="wow bounceInDown" data-wow-delay="0.4s">
								<div className="section-heading">
								<h2>Skill</h2>
								<i className="fa fa-2x fa-angle-down"></i>
								</div>
								</div>
							</div>
						</div>
						</div>
					</div>
					<div className="container">
						<div className="row">
							<div className="col-lg-2 col-lg-offset-5">
								<hr className="marginbot-50" />
							</div>
						</div>
				        <div className="row">
				            <div className="col-sm-3 col-md-3">
								<div className="wow fadeInLeft" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons1} alt="" />
									</div>
									<div className="service-desc">
										<h5>PHP + MySQL</h5>
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInUp" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons2} alt="" />
									</div>
									<div className="service-desc">
										<h5>Javacript & Jquery</h5>
										
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInUp" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons3} alt="" />
									</div>
									<div className="service-desc">
										<h5>PostgreSQL</h5>
										
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInRight" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons4} alt="" />
									</div>
									<div className="service-desc">
										<h5>Html + Css</h5>
										
									</div>
				                </div>
								</div>
				            </div>
				        </div> <br/>
				        <div className="row">
				            <div className="col-sm-3 col-md-3">
								<div className="wow fadeInLeft" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons5} alt="" />
									</div>
									<div className="service-desc">
										<h5>CodeIgniter</h5>
										
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInUp" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons6} alt="" />
									</div>
									<div className="service-desc">
										<h5>Yii2 Framework</h5>
										
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInUp" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons7} alt="" />
									</div>
									<div className="service-desc">
										<h5>Boostrap</h5>
										
									</div>
				                </div>
								</div>
				            </div>
							<div className="col-sm-3 col-md-3">
								<div className="wow fadeInRight" data-wow-delay="0.2s">
				                <div className="service-box">
									<div className="service-icon">
										<img src={icons8} alt="" />
									</div>
									<div className="service-desc">
										<h5>Accounting System</h5>
										
									</div>
				                </div>
								</div>
				            </div>
				        </div>		
					</div>
				</section>
				<section id="portofolio" className="home-section text-center">
					<div className="heading-contact">
						<div className="container">
							<div className="row">
								<div className="col-lg-8 col-lg-offset-2">
									<div className="wow bounceInDown" data-wow-delay="0.4s">
										<div className="section-heading">
											<h2>Portofolio</h2>
											<i className="fa fa-2x fa-angle-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="container">
						<div className="row">
							<div className="col-lg-2 col-lg-offset-5">
								<hr className="marginbot-50" />
							</div>
						</div>
						<div className="row">
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="wow bounceInUp" data-wow-delay="0.2s">
						            <div className="team boxed-grey">
						                <div className="inner">
											<h5>Komuto</h5>
						                    <p className="subtitle">Komunitas Toko</p>
						                    <div className="avatar"><img src={portofolios1} alt="" className="img-responsive" /></div>
						                </div>
						            </div>
								</div>
					        </div>
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="wow bounceInUp" data-wow-delay="0.2s">
						            <div className="team boxed-grey">
						                <div className="inner">
											<h5>Perpustakaan</h5>
						                    <p className="subtitle">Sistem Informasi Perpustakaan</p>
						                    <div className="avatar"><img src={portofolios2} alt="" className="img-responsive" /></div>
						                </div>
						            </div>
								</div>
					        </div>
						</div> <br />
						<div className="row">
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="wow bounceInUp" data-wow-delay="0.2s">
						            <div className="team boxed-grey">
						                <div className="inner">
											<h5>ERP</h5>
						                    <p className="subtitle">Enterprise Resorce Planning</p>
						                    <div className="avatar"><img src={portofolios3} alt="" className="img-responsive" /></div>
						                </div>
						            </div>
								</div>
					        </div>
					        <div className="col-xs-12 col-sm-6 col-md-6">
								<div className="wow bounceInUp" data-wow-delay="0.2s">
						            <div className="team boxed-grey">
						                <div className="inner">
											<h5><a href="https://www.zonatik.com/" target="_blank">Zonatik</a></h5>
						                    <p className="subtitle">Zona Tiket Indonesia</p>
						                    <div className="avatar"><img src={portofolios4} alt="" className="img-responsive" /></div>
						                </div>
						            </div>
								</div>
					        </div>
						</div> <br />
						<div className="row">
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="wow bounceInUp" data-wow-delay="0.2s">
						            <div className="team boxed-grey">
						                <div className="inner">
											<h5><a href="http://www.competitivecatalog.org/" target="_blank">ComCat</a></h5>
						                    <p className="subtitle">Competitive Catalog</p>
						                    <div className="avatar"><img src={portofolios5} alt="" className="img-responsive" /></div>
						                </div>
						            </div>
								</div>
					        </div>
						</div>
					</div>
				</section>
				<section id="contact" className="home-section text-center">
					<div className="heading-contact">
						<div className="container">
							<div className="row">
								<div className="col-lg-8 col-lg-offset-2">
									<div className="wow bounceInDown" data-wow-delay="0.4s">
										<div className="section-heading">
											<h2>Contact</h2>
											<i className="fa fa-2x fa-angle-down"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="container">
						<div className="row">
							<div className="col-lg-2 col-lg-offset-5">
								<hr className="marginbot-50" />
							</div>
						</div>
						<div className="row">
				        	<div className="col-lg-8">
				            	<div className="boxed-grey">
				            		<form id="contact-form">
						                <div className="row">
						                    <div className="col-md-6">
						                        <div className="form-group">
						                            <label>Name</label>
						                            <input type="text" className="form-control" id="name" placeholder="Enter name" required="required" />
						                        </div>
						                        <div className="form-group">
						                            <label>Email Address</label>
						                            <div className="input-group">
						                                <span className="input-group-addon"><span className="glyphicon glyphicon-envelope"></span></span>
						                                <input type="email" className="form-control" id="email" placeholder="Enter email" required="required" />
						                            </div>
						                        </div>
						                        <div className="form-group">
						                            <label>Subject</label>
						                            <div className="input-group">
						                                <span className="input-group-addon"><span className="glyphicon glyphicon-envelope"></span></span>
						                                <input type="text" className="form-control" id="subject" placeholder="Enter Subject" required="required" />
						                            </div>
						                        </div>
						                    </div>
						                    <div className="col-md-6">
						                        <div className="form-group">
						                            <label>Message</label>
						                            <textarea name="message" id="message" className="form-control" rows="9" cols="25" required="required" placeholder="Message"></textarea>
						                        </div>
						                    </div>
						                    <div className="col-md-12">
						                        <button type="submit" className="btn btn-skin pull-right" id="btnContactUs">
						                            Send Message
						                        </button>
						                    </div>
						                </div>
						            </form>
				            	</div>
				            </div>
				            <div className="col-lg-4">
				            	<div className="widget-contact">
				            		<h5>Location</h5>
									<address>
										<strong>Jl. Jatimakmur No. 7</strong><br />
									  	Pondokgede Bekasi<br />
									  	Jawa Barat, Indonesia<br />
									  	
									</address>
									<address>
										<strong>Email</strong><br />
										<a id="emails" href="mailto:#">afifurrahman27@gmail.com</a>
									</address>
									<address>
									  	<strong>Were on social networks</strong><br />
				                       	<ul className="company-social">
				                            <li className="social-facebook"><a href="https://www.facebook.com/afief.vageteange/" target="_blank"><i className="fa fa-facebook"></i></a></li>
				                            <li className="social-dribble"><a href="https://www.instagram.com/afifur.r/" target="_blank"><i className="fa fa-instagram"></i></a></li>
				                        	<li className="social-linkedin"><a href="https://www.linkedin.com/in/afifur-rahman-1b358211b/" target="_blank"><i className="fa fa-linkedin"></i></a></li>
				                        </ul>	
									</address>	
				            	</div>
				            </div>
				        </div>
					</div>
				</section>
				<footer>
					<div className="container">
						<div className="row">
							<div className="col-md-12 col-lg-12">
								<div className="wow shake" data-wow-delay="0.4s">
									<div className="page-scroll marginbot-30">
										<a href="#intro" id="totop" className="btn btn-circle">
											<i className="fa fa-angle-double-up animated"></i>
										</a>
									</div>
								</div>
								<p>2017 - Afifur R</p>
							</div>
						</div>	
					</div>
				</footer>
		    </div>
	    );	
	}
}

export default Header;